# libraries
import discord
import json
import os
import re
import requests


from datetime import datetime
from discord.ext import commands
from dotenv import load_dotenv

# project
import embed
import channel as channel_list
import registration

load_dotenv()
ENTRY_CHANNEL = int(os.getenv("ENTRY_CHANNEL"))

EMOJI_BOOK_BLUE = os.getenv("EMOJI_BOOK_BLUE")
EMOJI_BOOK_EDIT = os.getenv("EMOJI_BOOK_EDIT")
EMOJI_BOOK_SEARCH = os.getenv("EMOJI_BOOK_SEARCH")

list_session = {} 


def init_sessions(): 
    #f = open("sessions/0.json", "r") 
    #list_session[0] = json.load(f)
    #f.close()
    return


async def print_entry_message(bot):
    channel = bot.get_channel(ENTRY_CHANNEL)
    await channel.purge()

    message = await channel.send(embed = embed.embed_start())
    await message.add_reaction(EMOJI_BOOK_BLUE)
    await message.add_reaction(EMOJI_BOOK_EDIT)
    await message.add_reaction(EMOJI_BOOK_SEARCH)


async def print_sessions(bot):
    for element in list_session.items():
        if element["type"] == "registration":
            await session.session_registration_print(element, bot)
        if element["type"] == "edit":
            print("edit session")
        if element["type"] == "search":
            print("search session")


async def create_session(bot, reaction, user):
    print("===================")
    print(user.id)
    print(user.name)
    # check input is correct
    if(reaction.emoji != EMOJI_BOOK_BLUE 
    and reaction.emoji != EMOJI_BOOK_EDIT 
    and reaction.emoji != EMOJI_BOOK_SEARCH):
        print("not the good input")
        await reaction.remove(user)
        return


    # check if user already have an active session
    print(list_session)
    if user.id in list_session:
        print("User already have an active session")
        await reaction.remove(user)
        return


    print("User do not have an active session. Creating one.")

    #new_idx = len(list_session)

    new_sess = {}
    new_sess["user_id"] = user.id
    new_sess["user_name"] = user.name

    #create the private channel in DM & add channel ID to session
    
    channel = await user.create_dm()
    await channel.send(f"Hello {user.name}, a new session is starting.")
    new_sess["channel_id"] = channel.id
    if reaction.emoji == EMOJI_BOOK_BLUE:
        new_sess["type"] = "registration"
    if reaction.emoji == EMOJI_BOOK_EDIT:
        new_sess["type"] = "edit"
        await channel.send(f"Désolé, cette fonctionnalité n'est toujours pas créée.")
        await reaction.remove(user)
        return
    if reaction.emoji == EMOJI_BOOK_SEARCH:
        new_sess["type"] = "search"
        await channel.send(f"Désolé, cette fonctionnalité n'est toujours pas créée.")
        await reaction.remove(user)
        return


    new_sess["private"] = "True" 
    new_sess["step"] = 1 
    new_sess["book"] = {  
        "title" : "",
        "authors" : {},
        "date" : "",
        "categories" : {},
        "channel_id" : "",
        "description" : "",
        "img_cover" : "",
        "recommended_by" : "",
        "file" : ""
    	}


    # list_session.append(new_sess)
    list_session[user.id] = new_sess
    print(list_session)

    await registration.session_print(new_sess, bot)

    await reaction.remove(user)



def close_session(session):
    print(session)
    print(list_session)
    if session["user_id"] not in list_session:
        print("This session does not exists")
        return
    list_session.pop(session["user_id"])



async def dispatch_reaction_input(bot, reaction, user):

    if user.id not in list_session:
        print("This session is not active.")
        return

    if list_session[user.id]["type"] == "registration":
        await registration.session_input_reaction(list_session[user.id], reaction, user, bot)
    elif list_session[user.id]["type"] == "edit":
        print("reaction for an edit session")
        #edit.session_input_reaction(list_session[user.id], reaction, user, bot)
    elif list_session[user.id]["type"] == "search":
        print("reaction for a search session")
        #search.session_input_reaction(list_session[user.id], reaction, user, bot)


async def dispatch_message_input(bot, message):

    if message.author.id not in list_session:
        print("This session is not active.")
        return

    if list_session[message.author.id]["type"] == "registration":
        await registration.session_input_message(list_session[message.author.id], message, bot)
    elif list_session[message.author.id]["type"] == "edit":
        print("reaction for an edit session")
        #edit.session_input_reaction(list_session[message.author.id], message, bot)
    elif list_session[message.author.id]["type"] == "search":
        print("reaction for a search session")
        #search.session_input_reaction(list_session[message.author.id], message, bot)





