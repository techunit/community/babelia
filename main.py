# libraries
import discord
import os

from discord.ext import commands
from dotenv import load_dotenv

# project
import session as session_manager
import channel as channel_list



# define  variables for the project 

load_dotenv()

token=os.getenv("TOKEN")
ENTRY_CHANNEL = int(os.getenv("ENTRY_CHANNEL"))



# intents = discord.Intents.default()
intents = discord.Intents.all()
print(intents)

"""
intents.guilds = True
intents.members = True
intents.messages = True
intents.reactions = True
intents.emojis = True
intents.guild_messages = True
intents.guild_reactions = True
intents.dm_messages = True
intents.dm_reactions = True
"""


bot = commands.Bot(command_prefix=".", intents=intents)


"""
 Fonctions gérant la réception de signal 

"""



@bot.event
async def on_ready():
    print("Hello, world!\n")
    print("Babelia est connecté avec le nom : " + bot.user.name)
    print("Identifiant du bot : " + str(bot.user.id))
    print("---------")

    session_manager.init_sessions()
    # await session_manager.print_sessions(bot)

    await channel_list.load_channels(bot)

    await session_manager.print_entry_message(bot)




@bot.event
async def on_message(message):
    if message.author.id == bot.user.id:
        return


    if message.content == '!babelia':
        print("Affichage des commandes possilbes.")

    elif message.content == "!babelia list":
        await channel_list.list_all(message.channel)


    if message.channel.id == ENTRY_CHANNEL:
        await message.delete()
    else:
        await session_manager.dispatch_message_input(bot, message)



    if message.channel.type == "private":
        print(f"user {message.author.name} discute en message privé")


@bot.event
async def on_reaction_add(reaction, user):
    if user.id == bot.user.id:
        return
    
    print(f"user : {user}") 
    print(f"reaction: {reaction}") 


    if reaction.message.channel.id == ENTRY_CHANNEL:
        await session_manager.create_session(bot, reaction, user)
    else:
        await session_manager.dispatch_reaction_input(bot, reaction, user)


    if reaction.message.channel.type == "private":
        print(f"user {user.name} ajout réaction en message privé")



@bot.event
async def on_reaction_remove(reaction, user):
    if user.id == bot.user.id:
        return


    # if reaction.message.channel.id == s_enregistre: 
        # print(f"user {reaction.message.author.id} retire réaction dans le tchat enregistrement")
        # call biblia.session_enregistre_retire_reaction(message.author.id, reaction.emoji)


    #if reaction.message.channel.id == s_recherche: 
        # print(f"user {reaction.message.author.id} retire réaction dans le tchat recherche")
        # call biblia.session_recherche_retire_reaction(message.author.id, reaction.emoji)

    if reaction.message.channel.type == "private":
        print(f"user {reaction.message.author.id} retire réaction en message privé")
        # call biblia.session_privee_retire_reaction(message.author.id, reaction.emoji, reaction.message.channel.id)




bot.run(token)

