import discord
import channel as channel_list

"""
arts ---------------------- rose        #f54b9a
biographie ---------------- violet      #d751ff
économie ------------------ jaune       #e6ca18
environnement ------------- vert clair  #4dd84c
géopolitique -------------- mauve       #df4fd0
histoire ------------------ bleue foncé #7c6bff
informatique -------------- vert foncé  #31ac30
philosophie --------------- turquoise   #42aae4
santé --------------------- rouge       #e74040
sciences-physiques -------- marron      #e07039
sciences-sociales --------- beige       #f3dc72
société ------------------- menthe      #1de0aa
spiritualité -------------- orange      #ff9617
roman --------------------- bleue       #5388ff
autre ---------------------- gris       #a7a7a7

'🏯', '👴', '💰', '🌍', '🔮', '📜', '💻', '💭', '🐍', '🔬', '🔎', '🏠', '🌀', '📘', '❔'
"""

category_color = {
    '🏯' : '0xf54b9a',
    '👴' : '0xd751ff',
    '💰' : '0xe6ca18',
    '🌍' : '0x4dd84c',
    '🔮' : '0xdf4fd0',
    '📜' : '0x7c6bff',
    '💻' : '0x31ac30',
    '💭' : '0x42aae4',
    '🐍' : '0xe74040',
    '🔬' : '0xe07039',
    '🔎' : '0xf3dc72',
    '🏠' : '0x1de0aa',
    '🌀' : '0xff9617',
    '📘' : '0x5388ff',
    '❔' : '0xa7a7a7'
    }





def create_embed(book):

    embed = discord.Embed()


    # title related stuff
    # if book["title"]:
    # if book["date"]:
    embed.title = ""

    if book["authors"]:
        embed.title = embed.title + book["authors"][0]
        for i in range (1, len(book["authors"])):
            embed.title = embed.title + ", " + book["authors"][i] 

    if book["title"]:    
        embed.title = embed.title + "\n\n« " + book["title"] + " » "
    if book["date"]:
        embed.title = embed.title + " (" + book["date"] + ")"




    # description related stuff 
    embed.description = ""

    if book["description"]:
        embed.description = embed.description + book["description"] + "\n\n\n"

    if book["categories"]:
        embed.description = embed.description + "["
        print("-----------------")
        print("book content : ")
        print(book["categories"][0])
        print(book["categories"][0][0])
        print(book["categories"])
        print("=================")
        lib_icon = book["categories"][0][0]
        lib_idx = channel_list.list_emojis.index(lib_icon)
        lib_id = channel_list.list_channels[lib_idx].id
        embed.description = embed.description + f"<#{lib_id}>"

        for i in range (1, len(book["categories"])):
            embed.description = embed.description + " ; " + book["categories"][i]
            
        embed.description = embed.description + "]"


        print(f'icon is : {book["categories"][0][0]}')
        print(f'data is : {category_color}')
        print(f'value is : {category_color[book["categories"][0][0]]}')
        embed.color = int(category_color[book["categories"][0][0]], 16)
        #embed.color = 0x992d22

    if book["recommended_by"]:
        embed.description = embed.description + "\n\n🎬 Cité lors d'une interview par : **" + book["recommended_by"] + "**"

    # image stuff 
    print(f"EMBED : imgcover : {book['img_cover']}")
    if book["img_cover"]:
        print("img cover enter in")
        embed.set_image(url=book["img_cover"])

    return embed



def embed_start():
    embed = discord.Embed()
    embed.title = "Biblia"
    embed.description="« *Que voulez-vous faire ?* »\n\n\t📘 - **Ajouter** un ouvrage\n\t📝 - **Modifier** un ouvrage\n\t🔍 - **Rechercher** un ouvrage\n"
    embed.set_thumbnail(url="https://cdn.discordapp.com/icons/753277958881280210/6312238a02fa8c4f70d88acef6cca208.webp?size=128")
    return embed

