import pandas as pd
import os

dossier_data = os.path.dirname(os.path.abspath(__file__))



class Biblia():

    def __init__(self):
        self.chemin_bibliotheque = os.path.join(dossier_data,"Biblia.csv")
        self.chemin_registre = os.path.join(dossier_data,"registre.csv")
        if(os.path.exists(self.chemin_bibliotheque)):
            self.data = self._read_bibliotheque()
        else:
            self.data = "none"



    def ajouter_livre(self,dictionnaire):
        """
        ajoute un livre a la bibliotheque

        :param dictionnaire: dictionnaire des donné a ajouter a la bibliotheque
        """
        if(not os.path.exists(self.chemin_bibliotheque)):
            self._creation_csv_data(dictionnaire,self.chemin_bibliotheque)
        else:
            self.add = pd.DataFrame(data=[dictionnaire])
            self.data = self._read_bibliotheque()
            self.data = pd.concat([self.data,self.add],ignore_index=True)
            self._save_data(self.data,self.chemin_bibliotheque)

    def supprimer_livre(self,id_message):

        """
        supprime un livre de la database par sont id

        :param id_message: id du message contenant la fiche du livre
        """

        self.data = self._read_bibliotheque()
        self.data["index"] = self.data["id_message"]
        self.data.set_index("index", inplace=True)
        self.data.drop(labels=[id_message],axis=0, inplace=True)
        self._save_data(self.data,self.chemin_bibliotheque)

    def modifier_livre(self,id_message,dictionnaire,id_type_message=True):

        """
        permet de modifier une fiche dans la data de la bibliotheque

        :param id_message: id qui vas servir a selctionner la fiche dans la bibliotheque
        :param dictionnaire: dictionnaire contenant les nouvelles données a intergré a la bibliotheque
        :param id_type_message: true, selectionne par l'id du message/ false, par l'id du message miroire
        """

        if (not os.path.exists(self.chemin_bibliotheque)):
            return
        self.data = self._read_bibliotheque()

        if(id_type_message == True):
            self.data["index"] = self.data["id_message"]
        else:
            self.data["index"] = self.data["id_message_miroire"]

        self.data.set_index("index", inplace=True)

        self.data.loc[id_message, "id_message"] = dictionnaire["id_message"]
        self.data.loc[id_message, "id_channel"] = dictionnaire["id_channel"]
        self.data.loc[id_message, "id_serveur"] = dictionnaire["id_serveur"]
        self.data.loc[id_message, "id_message_miroire"] = dictionnaire["id_message_miroire"]
        self.data.loc[id_message, "url_publication"] = dictionnaire["url_publication"]
        self.data.loc[id_message, "title"] = dictionnaire["title"]
        self.data.loc[id_message, "authors"] = dictionnaire["authors"]
        self.data.loc[id_message, "date"] = dictionnaire["date"]
        self.data.loc[id_message, "categories"] = dictionnaire["categories"]
        self.data.loc[id_message, "description"] = dictionnaire["description"]
        self.data.loc[id_message, "url_img_cover"] = dictionnaire["url_img_cover"]
        self.data.loc[id_message, "url_img_author"] = dictionnaire["url_img_author"]
        self.data.loc[id_message, "recommended_by"] = dictionnaire["recommended_by"]
        self.data.loc[id_message, "contributor_id"] = dictionnaire["contributor_id"]


    def rechercher_livres(self):
        pass


    def rechercher_un_livre(self,id_livre):
        if(id_livre in self.return_liste_id_message()):
            self.data = self._read_bibliotheque()
            self.data["index"] = self.data["id_message"]
            self.data.set_index("index", inplace=True)
            return self.data.loc[id_livre].to_dict()
        else:
            return {}


    def return_liste_id_message(self):
        if(not os.path.exists(self.chemin_bibliotheque)):
            return []

        self.data = self._read_bibliotheque()
        return self.data.iloc["id_message"].to_list()


    def _read_bibliotheque(self):

        """
        recupere le dataframe via le fichier csv

        :return: dataframe de la bibliotheque
        """
        if (not os.path.exists(self.chemin_bibliotheque)):
            return
        self.data = pd.read_csv(self.chemin_bibliotheque)
        return self.data


    def _creation_csv_data(self,dictionnaire,chemin):
        """
        créé le dataframe et le fichier csv

        :param dictionnaire: dictionnaire des donnéé a entréé dans le dataframe lors de ca creation
        :param chemin: chemin ou le csv doit etre créé
        """
        self.data = pd.DataFrame(data=[dictionnaire])
        self._save_data(self.data,chemin)

    def _save_data(self, data,chemin):
        """
        enregistre le datafram de la bibliotheque

        :param data: Datafram a enregistrer
        :param chemin: chemin ou le csv doit etre créé
        """
        data.to_csv(chemin,index=False)


    """ +-----------------------------------------------------------------+  index perso   +-----------------------------------------------------------------+"""

    def new_react_registre(self,user_id:int,message_id:int,lu=True):
        self.dict = self.rechercher_un_livre(id_livre=message_id)

        if (self.dict == {}):
            return

        if(not os.path.exists(self.chemin_registre)):
            self.registre = pd.DataFrame(columns=["id_message","id_user","lu","title","autheur","date","categories","contributeur_id","intervenant_tkw"])
            self.labell = 0

        elif(self._registre_exist(user_id,message_id) != "none"):
            self.registre = self._read_registre_and_index()
            self.registre.loc[user_id,message_id]["lu"] = lu

        else:
            self.registre = pd.read_csv(self.chemin_registre)
            self.labell, y = self.registre.shape

            self.registre.loc[self.labell]["id_message"] = message_id
            self.registre.loc[self.labell]["id_user"] = user_id
            self.registre.loc[self.labell]["lu"] = lu
            self.registre.loc[self.labell]["title"] = self.dict["title"]
            self.registre.loc[self.labell]["autheur"] = self.dict["author"]
            self.registre.loc[self.labell]["annee"] = self.dict["date"]
            self.registre.loc[self.labell]["categories"] = self.dict["categories"]
            self.registre.loc[self.labell]["contributeur_id"] = self.dict["contributor_id"]
            self.registre.loc[self.labell]["intervenant_tkw"] = self.dict["recommended_by"]

        self._save_data(self.registre,self.chemin_registre)


    def remove_react_registre(self, user_id, message_id):
        self.registre = self._read_registre_and_index()
        self.registre.drop(labels=[user_id,message_id], inplace=True)
        self._save_data(self.registre,chemin=self.chemin_registre)

    def _registre_exist(self,user_id,message_id):
        try:
            self.registre = self._read_registre_and_index()
            return self.registre.loc[user_id,message_id]["lu"]

        except:
            return "none"

    def _read_registre_and_index(self):
        self.registre = pd.read_csv(self.chemin_registre)
        self.registre["index_message_id"] = self.registre["id_message"]
        self.registre["index_user_id"] = self.registre["id_user"]
        self.registre.set_index(["index_user_id", "index_message_id"])
        return self.registre



biblia = Biblia()
