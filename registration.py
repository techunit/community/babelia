# libraries
import discord
import json
import os
import re
import requests


from datetime import datetime
from discord.ext import commands
from dotenv import load_dotenv

# project
import session as session_manager
import embed
import channel as channel_list
import bibliotheque

load_dotenv()

# charge les emojis utilisés pour interragir
EMOJI_BOOK_ORANGE=os.getenv("EMOJI_BOOK_ORANGE")
EMOJI_ONE=os.getenv("EMOJI_ONE")
EMOJI_TWO=os.getenv("EMOJI_TWO")
EMOJI_THREE=os.getenv("EMOJI_THREE")
EMOJI_FOUR=os.getenv("EMOJI_FOUR")
EMOJI_FIVE=os.getenv("EMOJI_FIVE")
EMOJI_SIX=os.getenv("EMOJI_SIX")
EMOJI_SEVEN=os.getenv("EMOJI_SEVEN")
EMOJI_EIGHT=os.getenv("EMOJI_HEIGHT")
EMOJI_NINE=os.getenv("EMOJI_NINE")
EMOJI_ZERO=os.getenv("EMOJI_ZERO")
EMOJI_V=os.getenv("EMOJI_V")
EMOJI_X=os.getenv("EMOJI_X")
EMOJI_AUTOR=os.getenv("EMOJI_AUTOR")
EMOJI_TITLE=os.getenv("EMOJI_TITLE")
EMOJI_DATE=os.getenv("EMOJI_DATE")
EMOJI_DESC=os.getenv("EMOJI_DESC")
EMOJI_CATEGORY=os.getenv("EMOJI_CATEGORY")
EMOJI_ITW=os.getenv("EMOJI_ITW")
EMOJI_COVER=os.getenv("EMOJI_COVER")
EMOJI_FILE=os.getenv("EMOJI_FILE")

EMOJI_BOOK_BLUE=os.getenv("EMOJI_BOOK_BLUE")
EMOJI_BOOK_EDIT=os.getenv("EMOJI_BOOK_EDIT")
EMOJI_BOOK_SEARCH=os.getenv("EMOJI_BOOK_SEARCH")

CHANNEL_DUMP=os.getenv("CHANNEL_DUMP")

txt_enregistre = (  "« *Que voulez-vous faire ?* »\n\n\t📘 - **Ajouter** un ouvrage\n\t📝 - **Modifier** un ouvrage\n\t🔍 - **Rechercher** un ouvrage\n",
                    "Quelle information voulez vous modifier à la fiche du livre ?\n\t👤 - Auteur(s)\n\t📜 - Titre\n\t📅 - Année de publication\n\t📝 - Citation ou Résumé\n\t📂 - Catégorie\n\t🎬 - Cité en interview (optionnel)\n\t🖼️ - Image de couverture\n\t📖 - Fichier du livre\n\t✅ - Valider la publication\n\t❌ - Annuler la publication\n👇 Cliquez sur les réactions ci-dessous afin de renseigner leurs champs 👇\n\n",
                    "Renseignez le titre du livre",
                    "Renseignez le ou les auteurs (séparés par une virgule)",
                    "Renseignez l'**année** de publication",
                    "Renseignez la catégorie ou les catéogires (selectionnez l'émoji correspondant)",
                    "Renseignez une description rapide de l'œuvre",
                    "Fournissez une image de la couverture",
                    "Renseignez la personnalité interviewé chez Thinkerview ayant proposé ce livre",
                    "Envoyez moi le fichier .pdf ou .epub du livre"
                 )
txt_recherche = (   "Cliquez sur l'émoticone pour chercher un livre.",
                    "Quelle élément voulez-vous rechercher ?\n\t1-\ttitre\n\t2-\tauteur\n\t3-\tdate\n\t4-\tcategorie\n\t5-\tdescription\n\t6-\tproposé par (quelle itw thinkerview)",
                    "Selectionnez un titre"
                 )




list_emoji_step = {
    EMOJI_BOOK_BLUE: 1,
    EMOJI_AUTOR: 3,
    EMOJI_TITLE: 2,
    EMOJI_DATE: 4,
    EMOJI_DESC: 6,
    EMOJI_CATEGORY: 5,
    EMOJI_ITW: 8,
    EMOJI_COVER: 7,
    EMOJI_FILE: 9,
    EMOJI_V: 0,
    EMOJI_X: 0
    }





"""
def __init__(self):
    for file in os.listdir("sessions/"):
        print(f"opening file : {file}")
        path="sessions/" + file            
        f = open(path, "r")
        self.sessions[len(self.sessions)] = json.load(f)
        f.close()
"""

def reinit_session_add(session):
    # f = open("sessions/0.json", "r")
    # self.sessions[0] = json.load(f)
    # f.close()
    return




"""
Fonctions gérant l'envoie de texte

"""

async def session_print(session, bot):
    # print(session)
    # print(session["channel_id"])
    channel = bot.get_channel(session["channel_id"])

    # note : there is no simple way to purge DMChannel
    # await channel.purge()

    # Registration has two dialogs : 
    # 1 - asking which data to modify (or add)
    # 2 - asking for the value of it.
    # specific case : input for categories isn't a string but a reaction

    # 1
    if session["step"] == 1:
        await channel.send(embed = embed.create_embed(session["book"]))
        message = await channel.send(f"{txt_enregistre[session['step']]}")
        await message.add_reaction(EMOJI_AUTOR)
        await message.add_reaction(EMOJI_TITLE)
        await message.add_reaction(EMOJI_DATE)
        await message.add_reaction(EMOJI_DESC)
        await message.add_reaction(EMOJI_CATEGORY)
        await message.add_reaction(EMOJI_ITW)
        await message.add_reaction(EMOJI_COVER)
        await message.add_reaction(EMOJI_FILE)

        # Define the required field to validate the new entry. 
        # Bare minimum : title, author, categories, file
        if(session["book"]["title"] 
        and session["book"]["authors"] 
        and session["book"]["date"] 
        and session["book"]["description"] 
        and session["book"]["categories"] 
        and session["book"]["img_cover"] 
        and session["book"]["file"]):
            await message.add_reaction(EMOJI_V)

        await message.add_reaction(EMOJI_X)

    # 2
    elif session["step"] not in {0,1,5}:
        message = await channel.send(f"{txt_enregistre[session['step']]}")

    # 3
    elif session["step"] == 5:
        await channel_list.list_all(channel)
        message = await channel.send(f"{txt_enregistre[session['step']]}")
        for chan in channel_list.list_channels:
            await message.add_reaction(chan.name[0])

    else: 
        message = await channel.send("Cas impossible ou non géré. @zilot debug ça.")



"""
 Manage reaction input for recording session

"""

async def session_input_reaction(session, reaction, user, bot):
    if session["step"] not in {1,5}:
        # await reaction.remove(user)
        return

    if (session["step"] == 1 and (reaction.emoji not in list_emoji_step or reaction.emoji == EMOJI_BOOK_BLUE)):
        # await reaction.remove(user)
        return

    if (session["step"] == 5 and (reaction.emoji not in channel_list.list_emojis)):
        # await reaction.remove(user)
        return


    if session["step"] == 1:
        if reaction.emoji == EMOJI_V:
            print("livre enregistré.")
            await publish_book(session, bot)
            # await reaction.remove(user)
            await reaction.message.channel.send("Le livre a été publié. Fermeture de cette session.")
            session_manager.close_session(session)
            return

        if reaction.emoji == EMOJI_X:
            print("annulation de l'enregistrement")
            # await reaction.remove(user)
            await reaction.message.channel.send("Session annulée. Fermeture de cette session.")
            session_manager.close_session(session)
            return

        session["step"] = list_emoji_step[reaction.emoji]

        print("Result : ")
        print(session)

        await session_print(session, bot)
        return

    elif session["step"] == 5:
        print(f"user clicked on : {reaction.emoji}")

        chan_name = str(channel_list.list_channels[channel_list.list_emojis.index(reaction.emoji)])
        session["book"]["channel_id"] = channel_list.get_channel_id(chan_name)
        session["book"]["categories"][0] = chan_name
        
        session["step"] = list_emoji_step[EMOJI_BOOK_BLUE]
        print("Result : ")
        print(session)
        await session_print(session, bot)
        return




"""
 Manage text input for recording session

"""

async def session_input_message(session, message, bot):
            
    # II : étape attendant du texte (2 à N)
    if session["step"] in {1,5}:
        # await message.delete()
        print("Pas de message ici")
        return



    
    # sanitize la saisie, on retire ce dont on ne veut pas :
    # sanitize input, remove what we don't want : 
    # return, useless empty spaces start & end of input + arround , or ;
    # exception for the 6th step, the description.
    if session["step"] != 6:
        saisie = re.sub("\n", "", message.content)
        saisie = re.sub("^[ ]*|[ ]*$", "", saisie)


    if session["step"] == 2:
        session["book"]["title"] = saisie

    if session["step"] == 3:
        # there can be various authors for the same book.
        saisie = re.sub("[ ]*[,|;][ ]*", ",", message.content)
        authors = re.split(",|;", message.content)
        if "" in authors:
            authors.remove("")
        if " " in authors:
            authors.remove(" ")
        session["book"]["authors"] = authors

    if session["step"] == 4:
        # date must be a year [-3000 ; $current_year]
        if saisie.lstrip('-+').isdigit():
            pub_year = int(saisie)
            if pub_year <= datetime.now().year and pub_year >= -3000:
                session["book"]["date"] = saisie
            else:
                print("date trop ancienne ou dans le futur")
        else:
            print("Erreur, il faut une année")

    if session["step"] == 6:
        session["book"]["description"] = message.content


    # image cover
    if session["step"] == 7:
        if message.attachments:
            print(f"{message}")
            if message.attachments[0].filename.endswith(tuple([".jpg", ".jpeg", ".png", ".gif", ".webp"])):
                img_file = await message.attachments[0].to_file()
                dump_channel = bot.get_channel(int(CHANNEL_DUMP))
                post = await dump_channel.send(file=img_file)
                session["book"]["img_cover"] = post.attachments[0].url
            else:
                await message.channel.send("Le livre doit être sous format .jpg, .png, .gif ou .webp")
        else:
            print("is not attachement. Is URL")
            regex = re.compile(
                        r'^(?:http|ftp)s?://' # http:// or https://
                        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|' #domain...
                        r'localhost|' #localhost...
                        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})' # ...or ip
                        r'(?::\d+)?' # optional port
                        r'(?:/?|[/?]\S+)$', re.IGNORECASE)

            if re.match(regex, message.content) is None:
                print("regex not good")
                return

            print("continue")
            request = requests.get(message.content)
            print("request done.")
            print(f"request done with : {message.content}")

            if request.status_code == 200:
                print('Web site exists')
                session["book"]["img_cover"] = message.content
            else:
                print('Web site does not exist')

    if session["step"] == 8:
        session["book"]["recommended_by"] = message.content


    if session["step"] == 9:
        if message.attachments:
            if message.attachments[0].filename.endswith(tuple([".pdf", ".epub"])):
                session["book"]["file"] = await message.attachments[0].to_file()
            else:
                await message.channel.send("Le livre doit être sous format .pdf ou .epub")
        else:
            print("il n'y a pas de fichier")
    


    # après une saisie de donnée, on revient toujours à l'étape 1 (demande quelle info)
    # on ignore donc les étapes 0 et 1. L'étape validation est impossible
    session["step"] = list_emoji_step[EMOJI_BOOK_BLUE]

    print("Result : ")
    print(session)
    await session_print(session, bot)
    return






async def publish_book(session, bot):
    print(f"Publication d'un livre...")
    print(session)
    id_channel = session["book"]["channel_id"]
    channel = bot.get_channel(id_channel)
    sent_message = await channel.send(embed = embed.create_embed(session["book"]), file=session["book"]["file"])
    session["book"]["id_message"] = sent_message.id
    bibliotheque.biblia.ajouter_livre(session["book"])

    return 




